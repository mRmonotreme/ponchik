import csv
import itertools
import multiprocessing
import threading
import numpy as np
import matplotlib.pyplot as plt
import tensorflow as tf
import tensorflow.contrib.slim as slim
import scipy.signal
import scipy.misc
import gym
import gym_chess
import os
import random

import chess
import chess.pgn

from random import choice
from time import sleep
from time import time

verbose = False
def update_target_graph(from_scope, to_scope):
    from_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, from_scope)
    to_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, to_scope)

    op_holder = []
    for from_var, to_var in zip(from_vars, to_vars):
        op_holder.append(to_var.assign(from_var))
    return op_holder

def discount(x, gamma):
    return scipy.signal.lfilter([1], [1, -gamma], x[::-1], axis=0)[::-1]

def normalized_columns_initializer(std=1.0):
    def _initializer(shape, dtype=None, partition_info=None):
        out = np.random.randn(*shape).astype(np.float32)
        out *= std / np.sqrt(np.square(out).sum(axis=0, keepdims=True))
        return tf.constant(out)
    return _initializer

class AC_Network():
    def __init__(self, s_size, scope, trainer):
        with tf.variable_scope(scope):
            self.inputs = tf.placeholder(shape=[ None, s_size ], dtype=tf.float32)
            self.imageIn = tf.reshape(self.inputs, shape=[ -1, 8, 8, 1 ])
            tmp_input = self.imageIn
            ks = [5, 5, 4, 3]
            nm = [4, 4, 8, 8]
            self.conv = None
            self.max = None

            for i in range(0, len(ks)):
                self.conv = slim.conv2d(activation_fn=tf.nn.relu, inputs=tmp_input, num_outputs=nm[i], kernel_size=ks[i])
                tmp_input = self.conv
                print("[TMP_INPUT]", tmp_input)
                self.max = slim.max_pool2d(inputs = self.conv, kernel_size = 2)
                if verbose:
                    print(self.conv, self.max, ks[i], nm[i])

            if verbose:
                print(self.conv)
            hidden = slim.fully_connected(slim.flatten(self.conv), 512, activation_fn=tf.nn.elu)
            
            self.value = slim.fully_connected(hidden, 1, 
                                                activation_fn=None, 
                                                weights_initializer=normalized_columns_initializer(1.0), 
                                                biases_initializer=None)
            
            if scope != 'global':
                self.target_v = tf.placeholder(shape=[None], dtype=tf.float32)
                self.advantages = tf.placeholder(shape=[None], dtype=tf.float32)

                self.value_loss = 0.5 * tf.reduce_sum(tf.square(self.target_v - tf.reshape(self.value, [-1])))
                self.entropy = -tf.reduce_sum(self.value * tf.log(tf.clip_by_value(self.value, 1e-10, 1.0)))##-tf.reduce_sum(self.value * tf.log(self.value + 1e-4))## getting nan
                self.loss = 0.5 * self.value_loss - self.entropy * 0.01

                self.local_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, scope)
                self.gradients = tf.gradients(self.loss, self.local_vars)
                self.var_norms = tf.global_norm(self.local_vars)
                grads, self.grad_norms = tf.clip_by_global_norm(self.gradients, 40.0)
                
                global_vars = tf.get_collection(tf.GraphKeys.TRAINABLE_VARIABLES, 'global')
                self.apply_grads = trainer.apply_gradients(zip(grads, global_vars))

class Worker():
    def __init__(self, name, s_size, trainer, model_path, global_episodes):
        self.name = "worker_" + str(name)
        self.number = name        
        self.model_path = model_path
        self.trainer = trainer
        self.global_episodes = global_episodes
        self.increment = self.global_episodes.assign_add(1)
        self.episode_rewards = []
        self.episode_lengths = []
        self.episode_mean_values = []
        self.summary_writer = tf.summary.FileWriter("train_" + str(self.number))
        self.local_AC = AC_Network(s_size, self.name, trainer)
        self.update_local_ops = update_target_graph('global', self.name)        
        
        self.actions = [0, 1, 2]
        self.env = gym.make('chess-v0')
        
    def train(self, global_AC, rollout, sess, gamma, bootstrap_value):
        rollout = np.array(rollout)
        observations = rollout[:, 0]
        rewards = rollout[:, 2]
        next_observations = rollout[:, 3]
        values = rollout[:, 5]
        if verbose:
            print("[ROLLOUT]", rollout)
            print("[OBSERVATIONS] ", observations)
            print("[REWARDS] ", rewards)
            print("[NEXT_OBSERVATION] ", next_observations)
            print("[VALUES] ", values)
        
        self.rewards_plus = np.asarray(rewards.tolist() + [bootstrap_value])
        discounted_rewards = discount(self.rewards_plus, gamma)[:-1]
        self.value_plus = np.asarray(values.tolist() + [bootstrap_value])
        advantages = rewards + gamma * self.value_plus[1:] - self.value_plus[:-1]
        advantages = discount(advantages, gamma)

        if verbose:
            print("[TRAIN] value_loss policy_loss entropy gradient_norks variable_norms apply_gradients with dict target_v : discounted_rewards inputs : vstack(obeservations) actions : actions advantages:advantages")

        v_l, e_l, g_n, v_n, _    = sess.run([self.local_AC.value_loss,  
                                               self.local_AC.entropy, 
                                               self.local_AC.grad_norms, 
                                               self.local_AC.var_norms, 
                                               self.local_AC.apply_grads], 
                                    feed_dict={self.local_AC.target_v:discounted_rewards, 
                                               self.local_AC.inputs:np.vstack(observations), 
                                               self.local_AC.advantages:advantages})
        if verbose:
            print("[TRAINABLE_VARS] ", sess.run(self.local_AC.local_vars))
        return v_l / len(rollout), e_l / len(rollout), g_n, v_n
        
    def work(self, max_episode_length, gamma, global_AC, sess, coord, saver):
        episode_count = sess.run(self.global_episodes)
        total_steps = 0
        print("Starting worker %d" % self.number)
        with sess.as_default(), sess.graph.as_default():                 
            while not coord.should_stop():
                sess.run(self.update_local_ops)
                episode_buffer = []
                episode_values = []
                move_bufffer = []
                episode_reward = 0
                episode_step_count = 0
                d = False
                
                if episode_count % 25 == 0 and episode_count > 0:
                    print(chess.pgn.Game.from_board(self.env.env))
                s = self.env.reset()
                possible_actions = s[1]
                s = np.reshape(s[0], (64))
                print("[WORKER] ", episode_count, end="\r")
                while d == False:
                    tmp_highest_value = -100
                    chosen_action = None
                    count = 0
                    if verbose:
                        print("[VALUE_DISTRIBUTION] ")
                    for act in possible_actions:
                        s_tmp, _, _ = self.env.alt_step(act)
                        tmp_value = sess.run(self.local_AC.value, feed_dict={self.local_AC.inputs: [np.reshape(s_tmp[0], (64))]})
                        if verbose:
                            print(act, tmp_value[0][0], end=" ")
                            if count % 5 == 0:
                                print()
                        if  tmp_value[0] > tmp_highest_value:
                            tmp_highest_value = tmp_value[0]
                            chosen_action = act
                        self.env.alt_reset()
                        count += 1
                    if verbose and count - 1 % 5 != 0:
                        print()
                    v = sess.run(self.local_AC.value, feed_dict={self.local_AC.inputs: [s]})
                    if verbose:
                        print("[VALUE] ", v[0][0])
                    move_bufffer.append(chosen_action)
                    s1, r, d, _ = self.env.step(chosen_action)
                    possible_actions = s1[1]
                    
                    if d == False:
                        s1 =  np.reshape(s1[0], (64))
                    else:
                        s1 = s[0]
                        
                    episode_buffer.append([s, chosen_action, r, s1, d, v[0, 0]])
                    episode_values.append(v[0, 0])

                    episode_reward += r
                    s = s1
                    
                    if len(episode_buffer) == 30 and d != True and episode_step_count != max_episode_length - 1:
                        v1 = sess.run(self.local_AC.value, feed_dict={self.local_AC.inputs: [s]})[0, 0]
                        v_l, e_l, g_n, v_n = self.train(global_AC, episode_buffer, sess, gamma, v1)
                        if verbose:
                            print("[VALUE_1] ", v1)
                            print("[VALUE_LOSS ENTROPY GRAD_NORMS VAR_NORMS] ", v_l, e_l, g_n, v_n)
                        episode_buffer = [] 
                        sess.run(self.update_local_ops)
                    if verbose:
                        self.env.render(mode='human')
                    episode_step_count += 1
                    if d == True:
                        total_steps += episode_step_count
                        break
                                            
                self.episode_rewards.append(episode_reward)
                self.episode_lengths.append(episode_step_count)
                self.episode_mean_values.append(np.mean(episode_values))
                
                if len(episode_buffer) != 0:
                    v_l, e_l, g_n, v_n = self.train(global_AC, episode_buffer, sess, gamma, 0.0)
                                
                if episode_count % 5 == 0 and episode_count != 0:
                    if episode_count % 50 == 0 and self.name == 'worker_0':
                        saver.save(sess, self.model_path+'/model-'+str(episode_count)+'.cptk')
                        print("Saved Model")

                    mean_reward = np.mean(self.episode_rewards[-5:])
                    mean_length = np.mean(self.episode_lengths[-5:])
                    mean_value = np.mean(self.episode_mean_values[-5:])
                    summary = tf.Summary()
                    summary.value.add(tag='Perf/Reward', simple_value=float(mean_reward))
                    summary.value.add(tag='Perf/Length', simple_value=float(mean_length))
                    summary.value.add(tag='Perf/Value', simple_value=float(mean_value))
                    summary.value.add(tag='Losses/Value Loss', simple_value=float(v_l))
                    summary.value.add(tag='Losses/Entropy', simple_value=float(e_l))
                    summary.value.add(tag='Losses/Grad Norm', simple_value=float(g_n))
                    summary.value.add(tag='Losses/Var Norm', simple_value=float(v_n))
                    self.summary_writer.add_summary(summary, episode_count)

                    self.summary_writer.flush()
                if self.name == 'worker_0':
                    sess.run(self.increment)
                episode_count += 1

max_episode_length = 300
gamma = .99 
s_size = 64
load_model = False
model_path = './model'

tf.reset_default_graph()

if not os.path.exists(model_path):
    os.makedirs(model_path)

with tf.device("/cpu:0"): 
    global_episodes = tf.Variable(0, dtype=tf.int32, name='global_episodes', trainable=False)
    trainer = tf.contrib.opt.NadamOptimizer(learning_rate=1e-4)
    master_network = AC_Network(s_size, 'global', None)
    global_sum = tf.summary.FileWriter('global')
    global_sum.add_graph(tf.get_default_graph())
    global_sum.flush()
    num_workers = 1 #multiprocessing.cpu_count() # Set workers ot number of available CPU threads
    workers = []
    for i in range(num_workers):
        workers.append(Worker(i, s_size, trainer, model_path, global_episodes))
    saver = tf.train.Saver(max_to_keep=2)

with tf.Session() as sess:
    coord = tf.train.Coordinator()
    if load_model == True:
        print('Loading Model...')
        ckpt = tf.train.get_checkpoint_state(model_path)
        saver.restore(sess, ckpt.model_checkpoint_path)
    else:
        sess.run(tf.global_variables_initializer())

    worker_threads = []
    for worker in workers:
        worker_work = lambda: worker.work(max_episode_length, gamma, master_network, sess, coord, saver)
        t = threading.Thread(target=(worker_work))
        t.start()
        worker_threads.append(t)
    coord.join(worker_threads)